﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ParamModel;
using Newtonsoft.Json;

namespace ExamDemo.Controllers
{
    public class UsersController : Controller
    {
        private ExamDemoDb db = new ExamDemoDb();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public string LoginDone(LoginModel loginModel)
        {
            var username = loginModel.Username.Trim();
            var password = loginModel.Password.Trim();

            dynamic result;

            if(username.Length > 0 && password.Length > 0)
            {
                var user = db.Users.Where(x => x.Username.Equals(username)).FirstOrDefault();

                if(user != null)
                {
                    if (user.Password.Equals(password))
                    {
                        Session["id"] = user.Id;
                        Session["username"] = user.Username;

                        result = new
                        {
                            code = 200,
                        };
                    }
                    else
                    {
                        result = new
                        {
                            code = 1000,
                            msg = "密码错误，请重新输入"
                        };
                    }
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名不存在，请注册"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名或密码不能为空"
                };
            }

            return JsonConvert.SerializeObject(result);
        }
        [HttpPost]
        public string RegisterDone(RegisterModel registerModel)
        {
            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var repassword = registerModel.ConfirmPassword.Trim();

            dynamic result;

            if(username.Length>0 && password.Length>0 && password.Equals(repassword))
            {
                var user = db.Users.Where(x => x.Username == username).FirstOrDefault();

                if(user != null)
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名已存在，请重新输入"
                    };
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password
                    });

                    db.SaveChanges();

                    result = new
                    {
                        code = 200
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名或密码不能为空，且两次密码要一致"
                };
            }
            return JsonConvert.SerializeObject(result);
        }

        [HttpPost]
        public string Logout()
        {  
            dynamic result;

            result = new
            {
                code = 200
            };

            return JsonConvert.SerializeObject(result);
        }
        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Password,CreatedTime,UpdatedTime")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(users);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(users);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Password,CreatedTime,UpdatedTime")] Users users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
