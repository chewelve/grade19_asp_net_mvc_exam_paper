﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamDemo.Data;
using ExamDemo.Models;
using ExamDemo.ViewModel;
using Newtonsoft.Json;

namespace ExamDemo.Controllers
{
    public class MessagesController : Controller
    {
        private ExamDemoDb db = new ExamDemoDb();

        // GET: Messages
        public ActionResult Index()
        {
            if(Session["id"] == null)
            {
                return Redirect("/users/login");  
            }
            else
            {
            var users = db.Users.ToList();
            var msgs = db.Messages.ToList();
            var coms = db.Comments.ToList();

            var msgList = new List<MessageViewModel>();

            foreach(var msg in msgs)
            {
                var comList = new List<CommentViewModel>();

                var tempCom = coms.Where(x => x.MsgId == msg.Id).ToList();

                foreach(var com in tempCom)
                {

                    comList.Add(new CommentViewModel
                    {
                        FromUsername = users.Where(x=>x.Id == com.FromUserId).FirstOrDefault().Username,
                        ToUsername = users.Where(x => x.Id == msgs.Where(m=>m.Id == com.MsgId).FirstOrDefault().FromUserId).FirstOrDefault().Username,
                        Comment = com.Comment
                    });
                }

                msgList.Add(new MessageViewModel
                {
                    Id = msg.Id,
                    Content = msg.Content,
                    CommentViewModels = comList
                });
            }

            var view = new SayViewModel()
            {
                Username = Session["username"] == null ? "" : Session["username"].ToString(),
                MessageViewModels = msgList
            };
            return View(view);
            }

        }
        [HttpPost]
        public string Say(string sayMsg)
        {
            dynamic result;

            var userId = Convert.ToInt32(Session["id"]);

            if (sayMsg.Trim().Length > 0)
            {
                db.Messages.Add(new Messages
                {
                    FromUserId = userId,
                    Content = sayMsg
                });

                db.SaveChanges();

                result = new
                {
                    code = 200,
                    content = sayMsg,
                    id = userId
                };

                return JsonConvert.SerializeObject(result);
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "内容不能为空"
                };

                return JsonConvert.SerializeObject(result);
            }
        }


        [HttpPost]
        public string Comment(string commentMsg,int msgId)
        {
            dynamic result;

            var fromuserId = Convert.ToInt32(Session["id"]);

            var toUser = db.Users.Where(x => x.Id == db.Messages.Where(m => m.Id == msgId).FirstOrDefault().FromUserId).FirstOrDefault().Username;

            if (commentMsg.Trim().Length > 0)
            {
                db.Comments.Add(new Comments
                {
                    FromUserId = fromuserId,
                    MsgId = msgId,
                    Comment = commentMsg
                });

                db.SaveChanges();

                result = new
                {
                    code = 200,
                    content = commentMsg,
                    fromUserName = Session["username"].ToString(),
                    toUserName= toUser,
                    msgId = msgId
                };

                return JsonConvert.SerializeObject(result);
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "内容不能为空"
                };

                return JsonConvert.SerializeObject(result);
            }
        }
        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Messages/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FromUserId,Content,CreatedTime,UpdatedTime")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(messages);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messages);
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FromUserId,Content,CreatedTime,UpdatedTime")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messages).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messages);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = db.Messages.Find(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Messages messages = db.Messages.Find(id);
            db.Messages.Remove(messages);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
