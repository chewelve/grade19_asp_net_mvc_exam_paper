﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ExamDemo.Models;

namespace ExamDemo.Data
{
    public class DataInit:DropCreateDatabaseIfModelChanges<ExamDemoDb>
    {
        protected override void Seed(ExamDemoDb db)
        {
            db.Users.AddRange(new List<Users>
            {
                new Users
                {
                    Username = "woozi",
                    Password = "111"
                },
                new Users
                {
                    Username = "scoups",
                    Password = "111"
                }
            });

            db.Messages.AddRange(new List<Messages>
            {
                new Messages
                {
                    FromUserId = 1,
                    Content = "你好"
                }
            });

            db.Comments.AddRange(new List<Comments>
            {
                new Comments
                {
                    FromUserId = 2,
                    MsgId = 1,
                    Comment = "hello"
                }
            });

            db.SaveChanges();
            base.Seed(db);
        }
    }
}