﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamDemo.ViewModel
{
    public class SayViewModel
    {
        public SayViewModel()
        {
            MessageViewModels = new List<MessageViewModel>();
        }
        public string Username { get; set; }
        public IEnumerable<MessageViewModel> MessageViewModels { get; set; }
    }
}